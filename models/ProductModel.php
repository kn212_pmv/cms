<?php

namespace models;

use core\Core;
use core\Model;
use core\Utils;

class ProductModel extends Model
{
    protected static $tableName = 'product';


    /*public static function addProduct($row){
        $fieldsList=['model', 'price', 'count', 'text','producer_id','size_id','category_id','visible'];
        $row= Utils::ArrayFilter($row,$fieldsList);
        Core::getInstance()->getDB()->insert(self::$tableName,$row);

    }*/

    public function addProducts($row)
    {

        $userModel = new \models\Users();
        $user = $userModel->GetCurrentUser();
        if ($user == null) {
            $result = [
                'error' => true,
                'messages' => ['Користувач не аунтентифікований']
            ];
            return false;
        }
        $validateResult =$this->Validate($row);
        if (is_array($validateResult)) {
            return [
                'error' => true,
                'messages' => $validateResult
            ];
        }
        $fields = ['model', 'price', 'count', 'text', 'producer_id', 'size_id', 'category_id', 'visible','photo'];
        $rowFiltered = Utils::ArrayFilter($row, $fields);
        if (!empty($_FILES['file']['type'])) {

            switch ($_FILES['file']['type']) {
                case 'image/jpeg':
                    $extension = 'png';
                    break;
                case 'image/png' :
                    $extension = 'jpg';
                    break;
            }
            do {
                $fileName = uniqid() . '.' . $extension;
                $newPath = "files/product/{$fileName}";
            } while (file_exists($newPath));
            move_uploaded_file($_FILES['file']['tmp_name'], $newPath);
            $rowFiltered['photo'] = $fileName;
        }



        $id = Core::getInstance()->getDB()->insert(self::$tableName, $rowFiltered);
        return [
            'error' => false,
            'id' => $id
        ];
    }



    public static function deleteProduct($id){
        Core::getInstance()->getDB()->delete(self::$tableName,[
            'id'=>$id
        ]);
        return true;
    }
    public static function updateProduct($id,$row){
        $fieldsList=['model', 'price', 'count', 'text','producer_id','size_id','category_id','visible'];
        $row= Utils::ArrayFilter($row,$fieldsList);
        Core::getInstance()->getDB()->update(self::$tableName,$row,[
            'id'=>$id
        ]);
        return true;

    }

    public function ChangePhoto($id)
    {
        $row = $this->GetProductById($id);
        $folder = 'files/products/';
        $photoPath = $folder . $row['photo'];
        if (is_file($photoPath))
            unlink($photoPath);
    }



    public static function getProductById($id){
        $row=Core::getInstance()->getDB()->select(self::$tableName,'*',[
            'id'=>$id
        ]);
        if(!empty($row))
            return[$row[0]];
        else
            return null;
    }
    public static function getProductsInCategory($category_id){
        $rows=Core::getInstance()->getDB()->select(self::$tableName,'*',[
            'category_id'=>$category_id
        ]);
        return $rows;
    }


    public  function Validate($row)
    {
        $errors = [];
        if (empty($row['model']))
            $errors [] = 'Поле "Заголовок новини" не може бути порожнім';
        if (empty($row['text']))
            $errors [] = 'Поле "Короткий текст новини" не може бути порожнім';
        if ($row['price'] <= 0)
            $errors [] = 'Поле "Ціна" не може бути порожнім';
        if ($row['count'] <= 0)
            $errors [] = 'Поле "К-сть товару" не може бути порожнім';
        if (empty($row['category_id']))
            $errors [] = 'Поле "Категорія" не може бути порожнім';
        if (count($errors) > 0)
            return $errors;
        else
            return true;
    }

    public static function GetLastProducts($count)
    {
        return Core::getInstance()->getDB()->select(self::$tableName, '*', null, ['datetime' => 'DESC'], $count);
    }

    public function getAllProducts(){
        return Core::getInstance()->getDB()->select(self::$tableName, '*');
    }



}