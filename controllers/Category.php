<?php

namespace controllers;

use core\Controller;
use models\CategoryModel;
use models\ProductModel;
use models\Users;


class Category extends Controller
{
    function __construct()
    {
        $this->usersModel = new \models\Users();
    }


    public function actionIndex()
    {
        $rows = CategoryModel::GetCategories();

        return $this->render('index', [

            'rows' => $rows


        ], [
            'MainTitle' => 'Категорії',
            //'PageTitle' => 'Новини',
        ]);
    }

    public function actionAdd()
    {

        if (!$this->usersModel->isAdmin())
            return $this->error(403);
        if ($this->isPost()) {
            $errors = [];
            $_POST['name'] = trim($_POST['name']);
            if (empty($_POST['name']))
                $errors['name'] = 'Назва категорії не вказана';
            if (empty($errors)) {
                CategoryModel::AddCategory($_POST['name'], $_FILES['file']['tmp_name']);
                return $this->redirect('/category/index');
            } else {
                $model = $_POST;
                return $this->render('add', [
                    'errors' => $errors,
                    'model' => $model
                ]);

            }

        }

        return $this->render('add');
    }

    public function actionEdit()
    {
        $id = $_GET['id'];


        if (!$this->usersModel->isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $category = CategoryModel::GetCategoryById($id);

            if ($this->isPost()) {
                $errors = [];

                $_POST['name'] = trim($_POST['name']);
                if (empty($_POST['name']))
                    $errors['name'] = 'Назва категорії не вказана';
                if (empty($errors)) {
                    CategoryModel::UpdateCategory($id, $_POST['name']);
                    if (!empty($_FILES['file']['tmp_name'])) {
                        CategoryModel::changePhoto($id, $_FILES['file']['tmp_name']);
                    }
                    return $this->redirect('/category/index');
                } else {
                    $model = $_POST;
                    return $this->render('add', [
                        'errors' => $errors,
                        'model' => $model,
                        'category' => $category
                    ]);

                }

            }


            return $this->render('edit', [
                'category' => $category
            ]);
        } else
            return $this->error(403);
    }

    public function actionDelete()
    {
        $id = $_GET['id'];

        if (!$this->usersModel->isAdmin())
            return $this->error(403);
        if ($id > 0) {
            $category = CategoryModel::GetCategoryById($id);

            if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {
                $filePath = 'files/category/' . $category['photo'];
                if (is_file($filePath))
                    unlink($filePath);
                CategoryModel::DeleteCategory($id);
                return $this->redirect('/category/');
            }

            return $this->render('delete', [
                'category' => $category
            ]);


        } else
            return $this->error(403);


    }

    public function actionView()
    {
        $id = $_GET['id'];
        $category = CategoryModel::GetCategoryById($id);
        $product = ProductModel::getProductsInCategory($id);
        return $this->render('view', [
            'category' => $category,
            'product' => $product
        ]);


    }


}