<?php

namespace controllers;

use core\Controller;
use models\CategoryModel;
use models\ProductModel;

class Product extends Controller
{
    protected $user;
    protected $newsModel;
    protected $userModel;
    public function __construct(){
        $this->userModel=new \models\Users();
        $this->productModel=new \models\ProductModel();
        $this->user=$this->userModel->GetCurrentUser();

    }
    public function actionIndex()
    {
        $products=$this->productModel->getAllProducts();
        return $this->render('index',[
            'products'=>$products
        ]);
    }

//    public function actionAdd()
//    {
//        $category_id = $_GET['id'];
//        if (empty($category_id))
//            $category_id = null;
//        $categories = CategoryModel::GetCategories();
//        if ($this->isPost()) {
//            $errors = [];
//            $_POST['model'] = trim($_POST['model']);
//            if (empty($_POST['model']))
//                $errors['model'] = 'Назва товару не вказана';
//            if (empty($_POST['category_id']))
//                $errors['category_id'] = 'Категорія не вибрана';
//            if ($_POST['price'] <= 0)
//                $errors['price'] = 'Ціна не вказана';
//            if ($_POST['count'] <= 0)
//                $errors['count'] = 'Кількість товару не вказана';
//            if (empty($errors)) {
//                ProductModel::addProduct($_POST);
//                return $this->redirect('/category/index');
//            } else {
//                $model = $_POST;
//                return $this->render('add', [
//                    'errors' => $errors,
//                    'model' => $model,
//                    'category_id' => $category_id,
//                    'categories' => $categories
//
//                ]);
//
//            }
//            ProductModel::addProduct($_POST);
//            return $this->redirect('/product');
//
//        }
//        return $this->render('add', [
//            'categories' => $categories,
//            'category_id' => $category_id
//        ]);
//
//    }

    public function actionAdd()
    {
        if (!empty($_GET['id']))
            $category_id = $_GET['id'];
        else
            $category_id = null;
        $categories = CategoryModel::GetCategories();
        $titleForbidden = 'Доступ заборонено';
        if (empty($this->user))
            return $this->render('forbidden', null, [
                'PageTitle' => $titleForbidden,
                'MainTitle' => $titleForbidden
            ]);
        $title = 'Додавання товару';
        if ($this->isPost()) {
            $result = $this->productModel->AddProducts($_POST);
            if ($result['error'] === false) {

                return $this->renderMessage('ok', 'Товар успішно додано', null, [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                ]);
            } else {
                $message = implode('<br/>', $result['messages']);
                return $this->render('add',
                    [
                        'model' => $_POST,
                        'category_id' => $category_id,
                        'categories' => $categories,

                    ],
                    [
                        'PageTitle' => $title,
                        'MainTitle' => $title,
                        'MessageText' => $message,
                        'MessageClass' => 'danger',
                    ]);
            }
        } else {
            $params =
                [
                    'PageTitle' => $title,
                    'MainTitle' => $title,
                ];
            return $this->render('add',
                [
                    'model' => $_POST,
                    'categories' => $categories,
                    'category_id' => $category_id
                ], $params);
        }
    }






    public function actionView()
    {
        $id = $_GET['id'];

        $product = ProductModel::getProductById($id);

        return $this->render('view', [
            'product' => $product
        ]);

    }

    public function actionEdit()
    {
        if (!empty($_GET['id']))
            $category_id = $_GET['id'];
        else
            $category_id = null;
        $categories = CategoryModel::GetCategories();
        $id = $_GET['id'];
        $title = 'Редагування вмісту товару';
        if ($this->isPost()) {
            $result = ProductModel::updateProduct($id, $_POST);
            if ($result === true) {


                return $this->renderMessage('ok', 'Товар успішно збережено', null, [
                    'PageTitle' => $title,
                    //'MainTitle' => $title,
                ]);
            } else {
                //$message = implode('<br/>', $result);
                return $this->render('add',
                    [
                        'categories' => $categories,
                        'category_id' => $category_id
                    ],
                    [
                        'PageTitle' => $title,
                        //'MainTitle' => $title,
                        //'MessageText' => $message,
                        'MessageClass' => 'danger',
                    ]);
            }
        } else {
            $params =
                [
                    'PageTitle' => $title,
                    //'MainTitle' => $title,
                ];
            return $this->render('add', [
                'model' => $_POST,
                'categories' => $categories,
                'category_id' => $category_id
            ], $params);
        }
    }

    /**
     * Видалення новини
     */
    public function actionDelete()
    {
        $title = 'Видалення товару';
        $id = $_GET['id'];
        if (isset($_GET['confirm']) && $_GET['confirm'] == 'yes') {
            if (ProductModel::deleteProduct($id))
                return $this->redirect('/category/');
            else
                return $this->renderMessage('error', 'Помилка видалення товару', null, [
                    'PageTitle' => $title,
                    'MainTitle' => $title,

                ]);
        }
        $news = ProductModel::getProductById($id);
        return $this->render('delete', ['model' => $news], [
            'PageTitle' => $title,
            'MainTitle' => $title
        ]);
    }


}