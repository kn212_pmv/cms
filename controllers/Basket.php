<?php

namespace controllers;

use core\Controller;
use models\BasketModel;

class Basket extends Controller
{
    public function actionIndex(){
        $basket=BasketModel::GetProductsInBasket();
        return $this->render('index',[
            'basket'=>$basket
        ]);
    }

    public function actionAdd(){
        $id=$_GET['id'];
        var_dump($id);
        BasketModel::addProduct($id);


    }

}