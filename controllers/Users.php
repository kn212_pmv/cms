<?php

namespace controllers;

use core\Controller;

class Users extends Controller
{
    protected $usersModel;


    function __construct()
    {
        $this->usersModel = new \models\Users();
    }

    function actionLogout(){
        $title = 'Вхід на сайт';
        unset($_SESSION['user']);
        return $this->renderMessage('ok', 'Ви вийшли з вашого акаунту', 'null', [
            'MainTitle' =>  $title,
            'PageTitle' =>  $title,

        ]);
    }

    function actionLogin(){

        if($this->usersModel->IsUserAuthenticated()){
            $this->redirect('/');
        }


        $title = 'Вхід на сайт';
        if(isset($_SESSION['user']))
            return $this->renderMessage('ok', 'Ви вже увійшли на сайт', 'null', [
                'MainTitle' =>  $title,
                //'PageTitle' =>  $title,

            ]);

        if ($this->isPost()) {

            $user=$this->usersModel->AuthUser($_POST['login'],$_POST['password']);

            if(!empty($user))
            {
                $_SESSION['user']=$user;
                return $this->renderMessage('ok', 'Ви успішно увійшли на сайт', 'null', [
                    'MainTitle' =>  $title,
                    //'PageTitle' =>  $title,

                ]);


            } else {

                return $this->render('login', null, [
                    'MainTitle' =>  $title,
                    //'PageTitle' =>  null,
                    'MessageText' => 'Неправильний логін або пароль',
                    'MessageClass' => 'danger'


                ]);
            }
        } else {
            $params = [
                'MainTitle' =>  $title,
                //'PageTitle' =>  $title,
            ];
            return $this->render('login', null, $params);

        }

    }

    function actionRegister()
    {
        if ($this->isPost()) {
            $result = $this->usersModel->AddUser($_POST);
            if ($result === true) {
                return $this->renderMessage('ok', 'Користувач успішно зареєстрований!', 'null', [
                    'MainTitle' => 'Реєстрація',
                    //'PageTitle' => 'Реєстрація користувача',

                ]);


            } else {
                $message = implode('<br/>',$result);
                return $this->render('register', null, [
                    'MainTitle' => 'Реєстрація',
                    //'PageTitle' => 'Реєстрація користувача',
                    'MessageText' => $message,
                    'MessageClass' => 'danger'


                ]);
            }
        } else {
            $params = [
                'MainTitle' => 'Реєстрація',
                //'PageTitle' => 'Реєстрація користувача',
            ];
            return $this->render('register', null, $params);

        }
    }
}










