<?php

namespace controllers;


use core\Controller;

/**
 * Контролер для модуля News
 */
class News extends Controller
{

    public function actionDisplay(){
        echo "News->display";
    }

    public function actionIndex(){
        return $this->render('index',null,[
            'MainTitle' => 'Новини',
            'PageTitle' => 'Новини',

        ]);
    }
    public function actionList(){
        return $this->render('list',null,[
            'MainTitle' => 'Список',
            'PageTitle' => 'Список',

        ]);
    }


}