<?php
/** @var array $basket */
?>

<h1>Кошик</h1>

<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Назва товару</th>
        <th>Вартість одиниці</th>
        <th>Кількість</th>
        <th>Загальна вартість</th>
    </tr>
    </thead>
    <?php
    $index=1;
    if (!empty($basket['products']))
     foreach ($basket['products'] as $row) : ?>
    <tr>
        <td><?=$index ?></td>
        <td><?=$row['product']['model'] ?></td>
        <td id="pricePerOne"><?=$row['product']['price'] ?> грн.</td>
        <td><input type="number" class="form-control" value="<?=$_SESSION['countbought']?>" max="<?=$row['product']['count'] ?>" шт.</td>
        <td><?=$row['product']['price']*$row['count'] ?> грн.</td>


    </tr>
    <?php
    $index++;
    endforeach;
    ?>
    <tfoot>
    <tr>
        <th></th>
        <th></th>
        <th>Загальна сума:</th>
        <th><?=$basket['total_price']?> грн.</th>
    </tr>
    </tfoot>

</table>

<form method="post" action="">
    <input type="submit" class="btn btn-danger" value="Очистити кошик" name="clear"
           id="clear">
</form>


<?php
if (array_key_exists('clear', $_POST))
    unset($_SESSION['basket']);
?>

<script>

    let $priceperone=document.getElementById('pricePerOne').value;


    console.log($priceperone);




</script>


