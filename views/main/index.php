<main>
    <link rel="stylesheet" href="/styles/style.css"/>
    <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://aws-obg-image-lb-2.tcl.com/content/dam/brandsite/region/in/blog/pc/detail/new-blogs---28th-july/2022-tv-shopping-guide-time-to-get-your-next-google-tv/TimetoGetThumweb.jpg"
                     class="d-block w-100 cv" alt="..." height="600px" width="100%">

                <div class="container">
                    <div class="carousel-caption text-start">
                        <h1>Найновіші моделі тільки тут.</h1>
                        <p>Щоб отримати найкращі ціни і знижки скоріше реєструйся.</p>
                        <p><a class="btn btn-lg btn-primary" href="users/register">Зареєструватись</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img  src="https://www.androidheadlines.com/wp-content/uploads/2022/04/TCL-2022-C-Series-TVs.jpeg"
                     class="d-block w-100 cv imagemainpage" alt="..." height="600px" width="100%">


                <div class="container">
                    <div class="carousel-caption">
                        <h1>Найкращі моделі ти знайдешь тільки тут.</h1>
                        <p>Постійним покупцям знижки.</p>
                        <p><a class="btn btn-lg btn-primary" href="category/">Подивитись</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img src="https://aws-obg-image-lb-2.tcl.com/content/dam/brandsite/region/in/blog/pc/detail/new-blog/Blog%20Why%20to%20buy%20Home.jpg"
                     class="d-block w-100 cv" alt="..." height="600px" width="100%">

                <div class="container">
                    <div class="carousel-caption text-end">
                        <h1>Усього один клік до омріяного телевізора.</h1>
                        <p>Найкріщі ціни тільки у нас. Обирай який потрібен</p>
                        <p><a class="btn btn-lg btn-primary" href="category/">Детальніше</a></p>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->




        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Найновіші технології які вражають.</h2>
                <p class="lead">Отримуй справжнє насолодження від перегляду контенту.</p>
            </div>
            <div class="col-md-5">
                <img src="files/product/63cb3413144a0.png"
                     class="d-block w-100 cv" alt="..." height="350px" width="100%">

            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7 order-md-2">
                <h2 class="featurette-heading">Сучасні моделі мають велечезні яскраві екрани.</span></h2>
                <p class="lead">Нові телевізори мають сучасний якісний звук для комфортного перегляду контенту різних жанрів.</p>
            </div>
            <div class="col-md-5 order-md-1">
                <img src="files/product/63cb34e8c1d9a.png"
                     class="d-block w-100 cv" alt="..." height="350px" width="100%">

            </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Найкрутіші технології якості звучання та картинки мають навіть бюджетні моделі.</h2>
                <p class="lead">Обирай свій новий телевізор щоб окунутись у новий рівень сприйняття контенту.</p>
            </div>
            <div class="col-md-5">
                <img src="files/product/63cb34678ece2.png"
                     class="d-block w-100 cv" alt="..." height="350px" width="100%">
            </div>
        </div>

        <hr class="featurette-divider">

        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->

    <!-- FOOTER -->
    <footer class="container">
        <p class="float-end"><a href="#">Back to top</a></p>
        <p>&copy; 2017–2021 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
    </footer>
</main>


<script src="/docs/5.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


