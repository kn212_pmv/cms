<?php
/** @var array $category */
/** @var array $products */

use models\Users;

$userModel = new Users();

?>

<h1 class="h3 mb-3 fw-normal text-center">Список товарів</h1>
<?php
$userModel = new models\Users();

if ($userModel->IsAdmin()) :; ?>
    <div class="mb-3">
        <a class="btn btn-success" href="/product/add">Додати товар</a>

    </div>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-4 g-4 categories-list">
    <?php foreach ($products as $product): ?>
    <div class="col">
        <a href="/product/view?id=<?= $product['id'] ?>">
            <div class="card">
                <div class="photo">
                    <?php $filePath = 'files/product/' . $product['photo']; ?>
                    <?php if (is_file($filePath)) : ?>
                        <img src="/files/product/<?= $product['photo'] ?>" class="card-img-top cv"  alt="...">
                    <?php else: ?>
                        <img src="/static/images/no_image.jpg" class="card-img-top cv" height="250px" alt="...">
                    <?php endif; ?>
                    <div class="card-body">
                        <h3><?= $product['name'] ?> </h3>
                        <?= $product['text']; ?>
                    </div>

                    <div class="card-body">
                        <?php if ($userModel->IsAdmin()) : ?>
                            <a href="/product/edit?id=<?= $product['id'] ?>" class="btn btn-primary">Редагувати</a>
                            <a href="/product/delete?id=<?= $product['id'] ?>" class="btn btn-danger">Видалити</a>
                        <?php endif; ?>
                    </div>
                </div>
        </a>
    </div>
</div>
<?php endforeach; ?>
</div>

