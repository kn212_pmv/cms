<?php
/** @var array $model */

?>

<div class="alert alert-danger" role="alert">
    <h4 class="alert-heading">Видалити категорію "<?=$model[0]['model']?>"</h4>
    <p>Після видалення категорії всі товари даної категорії до категорії <b>"Не визначено"</b></p>
    <hr>
    <p class="mb-0">
        <a href="/product/delete?id=<?=$model[0]['id']?>&confirm=yes" class="btn btn-danger">Видалити</a>
        <a href="/category" class="btn btn-success">Відмінити</a>

    </p>





</div>
