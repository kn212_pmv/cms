<?php
/** @var array $product */
/** @var array $category */
?>

<h1><?=$category['name']?></h1>
<?php
$userModel = new models\Users();

if ($userModel->IsAdmin()) :; ?>
    <div class="mb-3">
        <a class="btn btn-success" href="/product/add?id=<?=$category['id']?>">Додати товар</a>


    </div>
<?php endif; ?>
<div class="row row-cols-1 row-cols-md-4 g-4 ">
    <?php foreach ($product as $row) : ?>
        <div class="categories-list">
            <div class="col">
                <a href="/product/view?id=<?=$row['id']?>">
                    <div class="card">
                        <?php
                        $filePath = 'files/product/' . $row['photo']; ?>
                        <?php if (is_file($filePath)) : ?>
                            <img src="/<?= $filePath ?>" class="card-img-top " alt="...">
                        <?php else : ?>
                            <img src="/static/images/no_image.jpg" class="card-img-top " alt="...">
                        <?php endif; ?>
                        <div class="card-body ">
                            <h5 class="card-title"><?= $row['model'] ?></h5>
                        </div>
                        <?php
                        if ($userModel->IsAdmin()) :; ?>
                            <div class="card-body ">
                                <a class="btn btn-primary" href="/product/edit?id=<?= $row['id'] ?>">Редагувати</a>
                                <a class="btn btn-danger" href="/product/delete?id=<?= $row['id'] ?>">Видалити</a>

                            </div>
                        <?php endif; ?>
                    </div>
                </a>
            </div>
        </div>

    <?php endforeach; ?>
</div>
