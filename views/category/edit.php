<?php

/** @var array $category */
/** @var array $errors */
/** @var array $model */



?>


<h2>Редагування категорії</h2>
<form action="" method="post" enctype="multipart/form-data">
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">Назва категорії</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Назва" value="<?=$category['name'] ?>">
        <?php if(!empty($errors['name'])): ?>
            <div class="form-text text-danger "> <?=$errors['name']; ?></div>
        <?php endif; ?>
    </div>
    <div class="col-3">
    <?php
    $filePath = 'files/category/' . $category['photo']; ?>
    <?php if (is_file($filePath)) : ?>
        <img class="img-thumbnail card-img-top" src="/<?= $filePath ?>"  alt="...">
    <?php else : ?>
        <img class="img-thumbnail card-img-top" src="/static/images/no_image.jpg"  alt="...">
    <?php endif; ?>
    </div>
    <div class="mb-3">
        <label for="file" class="form-label">Оберіть нову фотографію для категорії</label>
        <input type="file" class="form-control"  id="file" name="file"  accept="image/jpeg"/>
    </div>
    <div>
        <button class="btn btn-primary" type="submit">Зберегти</button>
    </div>
</form>