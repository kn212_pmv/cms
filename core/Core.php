<?php

namespace core;


use controllers\Main;

/**
 * Ядро системи
 */
class Core
{
    private static $instance;
    private static $mainTemplate;
    private static $db;

    private function __construct()
    {
        spl_autoload_register('\core\Core::__autoload');
        self::$db = $db = new \core\DB(DATABASE_HOST, DATABASE_LOGIN, DATABASE_PASSWORD, DATABASE_BASENAME);


    }

    /**
     * Повертає екземпляр класу Core
     * @return Core
     */
    public static function getInstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new Core();
            return self::getInstance();
        } else
            return self::$instance;

    }

    public function getDB()
    {

        return self::$db;
    }

    /**
     * Ініціалізація системи
     */
    public function init()
    {
        session_start();

        self::$mainTemplate = new Template();
    }

    /**
     * Виконує основний процес роботи CMS-системи
     * @return void
     */
    public function run()
    {
        $path = $_GET['path'];
        $pathParts = explode('/', $path);
        $className = ucfirst($pathParts[0]);
        if (empty($className))
            $fullClassName = 'controllers\\Main';
        else
            $fullClassName = 'controllers\\' . $className;
        $methodName = ucfirst($pathParts[1]);
        if (empty($methodName))
            $fullMethodName = 'actionIndex';
        else
            $fullMethodName = 'action' . $methodName;
        $statuscode = 200;
        if (class_exists($fullClassName)) {

            $controller = new $fullClassName();
            if (method_exists($controller, $fullMethodName)) {
                $method = new \ReflectionMethod($fullClassName, $fullMethodName);
                $paramsArray = [];
                foreach ($method->getParameters() as $parameter) {
                    array_push($paramsArray, isset($_GET[$parameter->name]) ? $_GET[$parameter->name] : null);

                }
                $result = $method->invokeArgs($controller, $paramsArray);
                if (is_array($result)) {
                    self::$mainTemplate->setParams($result);
                }

                //$controller->$fullMethodName($paramsArray);
            } else {
                $statuscode = 404;
            }

        } else {
            $statuscode = 404;
        }
        $statusCodeType = intval($statuscode / 100);
        if ($statusCodeType == 4 || $statusCodeType == 5) {
            $mainController = new Main();

        }


    }


    /**
     * Завершення роботи системи та виведення результату
     * @return void
     */
    public function done()
    {
        self::$mainTemplate->display('views/layout/index.php');

    }

    /**
     * Автозавантаження класів
     * @param $className string Назва класу
     *
     */
    public static function __autoload($className)
    {
        $fileName = $className . '.php';
        if (is_file($fileName)) {
            include "$fileName";
        }

    }

}